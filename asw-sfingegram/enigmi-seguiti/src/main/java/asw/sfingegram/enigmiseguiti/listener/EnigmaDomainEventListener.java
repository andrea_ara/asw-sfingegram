package asw.sfingegram.enigmiseguiti.listener;

import asw.sfingegram.kafka.DomainEvent;
import asw.sfingegram.kafka.EnigmaServiceEventChannel;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EnigmaDomainEventListener {

    @Autowired
    private EnigmaDomainEventConsumer enigmaDomainEventConsumer;

    @KafkaListener(topics = EnigmaServiceEventChannel.channel)
    public void listen(ConsumerRecord<String, DomainEvent> record) throws Exception {
        log.info("RECEIVED EVENT: {}", record.toString());
        DomainEvent event = record.value();
        this.enigmaDomainEventConsumer.onEvent(event);
    }
}
