package asw.sfingegram.enigmiseguiti.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class EnigmaSeguito implements Comparable<EnigmaSeguito> {

    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue
    private Long id;
    private String utente;
    private Long enigmaId;
    private String autore;
    private String[] testo;
    private String tipo;
    private String titolo;

    public EnigmaSeguito(String utente, Long enigmaId, String autore, String[] testo, String tipo, String titolo) {
        this();
        this.utente = utente;
        this.enigmaId = enigmaId;
        this.autore = autore;
        this.testo = testo;
        this.tipo = tipo;
        this.titolo = titolo;
    }

    @Override
    public int compareTo(EnigmaSeguito enigmaSeguito) {
        return this.getId().compareTo(enigmaSeguito.getId());
    }
}
