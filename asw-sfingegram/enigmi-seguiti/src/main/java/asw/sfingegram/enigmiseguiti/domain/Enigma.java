package asw.sfingegram.enigmiseguiti.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


/* Enigma (in formato breve, senza soluzione). */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Enigma implements Comparable<Enigma> {

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue
	private Long id;

	private String autore; 
	private String tipo; 
	private String titolo; 
	private String[] testo;

	public Enigma(String autore, String tipo, String titolo, String[] testo){
		this();
		this.autore = autore;
		this.tipo = tipo;
		this.titolo = titolo;
		this.testo = testo;
	}

	@Override
	public int compareTo(Enigma other) {
		return this.id.compareTo(other.id); 
	}
	
}
