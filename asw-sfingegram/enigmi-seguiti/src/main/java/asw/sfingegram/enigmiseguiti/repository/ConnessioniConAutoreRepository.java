package asw.sfingegram.enigmiseguiti.repository;

import java.util.Collection;

import asw.sfingegram.enigmiseguiti.domain.ConnessioneConAutore;

import org.springframework.data.repository.CrudRepository;

public interface ConnessioniConAutoreRepository extends CrudRepository<ConnessioneConAutore, Long> {

	public Collection<ConnessioneConAutore> findAll();

	public Collection<ConnessioneConAutore> findByUtente(String utente);

	public Collection<ConnessioneConAutore> findByAutore(String autore);

}

