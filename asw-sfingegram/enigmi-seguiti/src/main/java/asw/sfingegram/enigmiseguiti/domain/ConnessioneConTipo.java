package asw.sfingegram.enigmiseguiti.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Entity
@Data @NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ConnessioneConTipo implements Comparable<ConnessioneConTipo>{

    @Id
    @GeneratedValue
	private Long id; 
	private String utente; 
	private String tipo;

    public ConnessioneConTipo(String utente, String tipo) {
        this.utente = utente;
        this.tipo = tipo;
    }

    @Override
	public int compareTo(ConnessioneConTipo connessioneConTipo) {
		return this.getId().compareTo(connessioneConTipo.getId());
	}
	
}
