package asw.sfingegram.enigmiseguiti.rest;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;

import asw.sfingegram.enigmiseguiti.domain.EnigmaSeguito;
import asw.sfingegram.enigmiseguiti.service.EnigmiSeguitiService;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class EnigmiSeguitiController {

	@Autowired 
	private EnigmiSeguitiService enigmiSeguitiService;

	/* Trova gli enigmi (in formato breve) degli utenti seguiti da utente. */ 
	@GetMapping("/enigmiseguiti/{utente}")
	public Collection<EnigmaSeguito> getEnigmiSeguiti(@PathVariable String utente) {
		Instant start = Instant.now();
		log.info("REST CALL: getEnigmiSeguiti {}", utente);
		Collection<EnigmaSeguito> enigmi = enigmiSeguitiService.getEnigmiSeguiti(utente);
		Duration duration = Duration.between(start, Instant.now()); 
		log.info("getEnigmiSeguiti {} (trovati {} enigmi in {} ms): {}", utente, enigmi.size(), duration.toMillis(), enigmi);
		return enigmi; 
	}
	
}
