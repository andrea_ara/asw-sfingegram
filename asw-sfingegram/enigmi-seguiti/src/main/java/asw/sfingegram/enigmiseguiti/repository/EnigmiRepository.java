package asw.sfingegram.enigmiseguiti.repository;


import java.util.Collection;

import asw.sfingegram.enigmiseguiti.domain.Enigma;

import org.springframework.data.repository.CrudRepository;



public interface EnigmiRepository extends CrudRepository<Enigma, Long>{

    public Collection<Enigma> findByTipoStartingWith(String tipo);

    public Collection<Enigma> findByAutore(String autore);


}
