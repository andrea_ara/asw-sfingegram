package asw.sfingegram.enigmiseguiti.listener;

import asw.sfingegram.enigmiseguiti.service.EnigmiSeguitiService;
import asw.sfingegram.kafka.DomainEvent;
import asw.sfingegram.kafka.EnigmaCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class EnigmaDomainEventConsumer {

    @Autowired
    private EnigmiSeguitiService enigmiSeguitiService;

    public void onEvent(DomainEvent event){
        log.info("PROCESSING EVENT: {}", event);
        if(event.getClass().equals(EnigmaCreatedEvent.class)){
            EnigmaCreatedEvent enigmaCreatedEvent = (EnigmaCreatedEvent) event;
            this.enigmiSeguitiService.createEnigma(enigmaCreatedEvent.getAutore(), enigmaCreatedEvent.getTitolo(), enigmaCreatedEvent.getTipo(), enigmaCreatedEvent.getTesto());
        }
        else log.info("UNKNOWN EVENT: {}", event);
    }
}
