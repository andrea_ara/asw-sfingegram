package asw.sfingegram.enigmiseguiti.service;

import java.util.Collection;

import asw.sfingegram.enigmiseguiti.domain.ConnessioneConAutore;
import asw.sfingegram.enigmiseguiti.domain.ConnessioneConTipo;
import asw.sfingegram.enigmiseguiti.domain.Enigma;
import asw.sfingegram.enigmiseguiti.domain.EnigmaSeguito;
import asw.sfingegram.enigmiseguiti.repository.ConnessioniConAutoreRepository;
import asw.sfingegram.enigmiseguiti.repository.ConnessioniConTipoRepository;
import asw.sfingegram.enigmiseguiti.repository.EnigmiRepository;
import asw.sfingegram.enigmiseguiti.repository.EnigmiSeguitiRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
//@Transactional
public class EnigmiSeguitiService {

	@Autowired
    private EnigmiSeguitiRepository enigmiSeguitiRepository;

	@Autowired
    private EnigmiRepository enigmiRepository;

	@Autowired
    private ConnessioniConAutoreRepository connessioniConAutoreRepository;

	@Autowired
    private ConnessioniConTipoRepository connessioniConTipoRepository;

    public Collection<EnigmaSeguito> getEnigmiSeguiti(String utente) {
        return enigmiSeguitiRepository.findByUtente(utente);
    }

    public void createEnigma(String autore, String titolo, String tipo, String[] testo){
        Enigma enigma = enigmiRepository.save(new Enigma(autore, tipo, titolo, testo));
        createEnigmaSeguitoDaEnigma(enigma);
    }

    private void createEnigmaSeguitoDaEnigma(Enigma enigma){
        Collection<ConnessioneConAutore> connConAutore = this.connessioniConAutoreRepository.findByAutore(enigma.getAutore());
        Collection<ConnessioneConTipo> connConTipo = this.connessioniConTipoRepository.findByTipo(enigma.getTipo());
        connConAutore.forEach(c->enigmiSeguitiRepository.save(new EnigmaSeguito(c.getUtente(), enigma.getId(), enigma.getAutore(), enigma.getTesto(), enigma.getTipo(), enigma.getTitolo())));
        connConTipo.forEach(c->enigmiSeguitiRepository.save(new EnigmaSeguito(c.getUtente(), enigma.getId(), enigma.getAutore(), enigma.getTesto(), enigma.getTipo(), enigma.getTitolo())));
    }

    public void createConnessioneConAutore(String utente, String autore) {
        ConnessioneConAutore connessioneConAutore = this.connessioniConAutoreRepository.save(new ConnessioneConAutore(utente, autore));
        createEnigmaSeguitoDaConnessioneConAutore(connessioneConAutore);
    }

    private void createEnigmaSeguitoDaConnessioneConAutore(ConnessioneConAutore connessioneConAutore) {
        Collection<Enigma> enigmi = this.enigmiRepository.findByAutore(connessioneConAutore.getAutore());
        enigmi.forEach(e -> enigmiSeguitiRepository.save(new EnigmaSeguito(connessioneConAutore.getUtente(), e.getId(), e.getAutore(), e.getTesto(), e.getTipo(), e.getTitolo())));
    }

    public void createConnessioneConTipo(String utente, String autore) {
        ConnessioneConTipo connessioneConTipo = this.connessioniConTipoRepository.save(new ConnessioneConTipo(utente, autore));
        createEnigmaSeguitoDaConnessioneConTipo(connessioneConTipo);
    }

    private void createEnigmaSeguitoDaConnessioneConTipo(ConnessioneConTipo connessioneConTipo) {
        Collection<Enigma> enigmi = this.enigmiRepository.findByTipoStartingWith(connessioneConTipo.getTipo());
        enigmi.forEach(e -> enigmiSeguitiRepository.save(new EnigmaSeguito(connessioneConTipo.getUtente(), e.getId(), e.getAutore(), e.getTesto(), e.getTipo(), e.getTitolo())));
    }

}
