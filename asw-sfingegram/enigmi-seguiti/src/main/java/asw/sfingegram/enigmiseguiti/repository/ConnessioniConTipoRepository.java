package asw.sfingegram.enigmiseguiti.repository;

import java.util.Collection;

import asw.sfingegram.enigmiseguiti.domain.ConnessioneConAutore;
import asw.sfingegram.enigmiseguiti.domain.ConnessioneConTipo;

import org.springframework.data.repository.CrudRepository;

public interface ConnessioniConTipoRepository extends CrudRepository<ConnessioneConTipo, Long> {

	public Collection<ConnessioneConTipo> findAll();

	public Collection<ConnessioneConTipo> findByUtente(String utente);

	public Collection<ConnessioneConTipo> findByTipo(String tipo);


}

