package asw.sfingegram.enigmiseguiti.listener;

import asw.sfingegram.enigmiseguiti.service.EnigmiSeguitiService;
import asw.sfingegram.kafka.ConnessioneConAutoreCreatedEvent;
import asw.sfingegram.kafka.ConnessioneConTipoCreatedEvent;
import asw.sfingegram.kafka.DomainEvent;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ConnessioneDomainEventConsumer {

    @Autowired
    private EnigmiSeguitiService enigmiSeguitiService;

    public void onEvent(DomainEvent event) {
        log.info("PROCESSING EVENT: {}", event);
        if(event.getClass().equals(ConnessioneConAutoreCreatedEvent.class)) {
            ConnessioneConAutoreCreatedEvent connessioneConAutoreCreatedEvent = (ConnessioneConAutoreCreatedEvent) event;
            this.enigmiSeguitiService.createConnessioneConAutore(connessioneConAutoreCreatedEvent.getUtente(), connessioneConAutoreCreatedEvent.getAutore());
        } else if(event.getClass().equals(ConnessioneConTipoCreatedEvent.class)) {
            ConnessioneConTipoCreatedEvent connessioneConTipoCreatedEvent = (ConnessioneConTipoCreatedEvent) event;
            this.enigmiSeguitiService.createConnessioneConTipo(connessioneConTipoCreatedEvent.getUtente(), connessioneConTipoCreatedEvent.getTipo());
        } else {
            log.info("UNKNOWN EVENT: {}", event);
        }
    }
}
