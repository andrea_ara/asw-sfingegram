package asw.sfingegram.enigmiseguiti.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Entity
@Data @NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ConnessioneConAutore implements Comparable<ConnessioneConAutore>{

    @Id
    @GeneratedValue
	private Long id; 
	private String utente; 
	private String autore;

    public ConnessioneConAutore(String utente, String autore) {
        this.utente = utente;
        this.autore = autore;
    }

    @Override
	public int compareTo(ConnessioneConAutore connessioneConAutore) {
	    return this.getId().compareTo(connessioneConAutore.getId());
	}
	
}
