package asw.sfingegram.enigmiseguiti.repository;

import java.util.Collection;

import asw.sfingegram.enigmiseguiti.domain.EnigmaSeguito;

import org.springframework.data.repository.CrudRepository;



public interface EnigmiSeguitiRepository extends CrudRepository<EnigmaSeguito, Long>{

    public Collection<EnigmaSeguito> findByUtente(String utente);

}
