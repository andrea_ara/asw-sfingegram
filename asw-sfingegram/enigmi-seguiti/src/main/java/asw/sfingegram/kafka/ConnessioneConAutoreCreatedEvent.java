package asw.sfingegram.kafka;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor @NoArgsConstructor
public class ConnessioneConAutoreCreatedEvent implements DomainEvent{



    private Long id;
    private String utente;
    private String autore;


}
