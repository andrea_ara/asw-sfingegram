package asw.sfingegram.kafka;

import lombok.Data;

@Data
public class EnigmaCreatedEvent implements DomainEvent{

    private Long id;
    private String autore;
    private String titolo;
    private String tipo;
    private String[] testo;


}
