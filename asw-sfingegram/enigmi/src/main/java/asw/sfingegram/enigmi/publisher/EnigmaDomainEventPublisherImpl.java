package asw.sfingegram.enigmi.publisher;

import asw.sfingegram.kafka.DomainEvent;
import asw.sfingegram.kafka.EnigmaServiceEventChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EnigmaDomainEventPublisherImpl implements EnigmaDomainEventPublisher{

    @Autowired
    private KafkaTemplate<String, DomainEvent> template;
    private String channel = EnigmaServiceEventChannel.channel;

    @Override
    public void publish(DomainEvent event) {

        log.info("PUBLISHING MESSAGE: {} ON CHANNEL {}", event, channel);

        template.send(channel, event);

    }
}
