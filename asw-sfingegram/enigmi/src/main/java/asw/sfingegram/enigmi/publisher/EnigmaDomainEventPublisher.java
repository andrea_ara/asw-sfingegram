package asw.sfingegram.enigmi.publisher;

import asw.sfingegram.kafka.DomainEvent;

public interface EnigmaDomainEventPublisher {

    public void publish(DomainEvent event);

}
