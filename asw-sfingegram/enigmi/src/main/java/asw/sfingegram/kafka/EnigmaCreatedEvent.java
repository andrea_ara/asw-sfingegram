package asw.sfingegram.kafka;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EnigmaCreatedEvent implements DomainEvent{

    private Long id;
    private String autore;
    private String titolo;
    private String tipo;
    private String[] testo;


}
