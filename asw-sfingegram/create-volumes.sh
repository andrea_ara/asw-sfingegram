#!/bin/bash

echo "$(tput setaf 2)CREATING VOLUMES...$(tput sgr 0)";
docker volume create --name=enigmi
docker volume create --name=connessioni
docker volume create --name=enigmi-seguiti
echo