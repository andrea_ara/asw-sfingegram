#!/bin/bash

echo "$(tput setaf 2)REMOVING VOLUMES...$(tput sgr 0)"
docker volume rm enigmi
docker volume rm connessioni
docker volume rm enigmi-seguiti
echo