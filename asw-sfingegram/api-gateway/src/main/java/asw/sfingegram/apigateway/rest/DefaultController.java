package asw.sfingegram.apigateway.rest;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultController {

	@GetMapping("/")
	public Collection<String> index() {
		return Arrays.asList(
			"http://localhost:8080/enigmi/swagger-ui/", 
			"http://localhost:8080/connessioni/swagger-ui/", 
			"http://localhost:8080/enigmi-seguiti/swagger-ui/", 
			"http://localhost:8080/actuator" 
		); 
	}
	
}
