package asw.sfingegram.kafka;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor @NoArgsConstructor
public class ConnessioneConTipoCreatedEvent implements DomainEvent{

    private Long id;
    private String utente;
    private String tipo;


}
