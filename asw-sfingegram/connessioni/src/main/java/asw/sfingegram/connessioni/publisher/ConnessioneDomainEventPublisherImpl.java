package asw.sfingegram.connessioni.publisher;

import asw.sfingegram.kafka.ConnessioneServiceEventChannel;
import asw.sfingegram.kafka.DomainEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ConnessioneDomainEventPublisherImpl implements ConnessionePublisher {

    @Autowired
    private KafkaTemplate<String, DomainEvent> template;
    private String channel = ConnessioneServiceEventChannel.channel;

    @Override
    public void publish(DomainEvent event) {

        log.info("PUBLISHING MESSAGE: {} ON CHANNEL {}", event, channel);

        template.send(channel, event);

    }

}
