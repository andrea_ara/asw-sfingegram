package asw.sfingegram.connessioni.publisher;

import asw.sfingegram.kafka.DomainEvent;

public interface ConnessionePublisher {

    public void publish(DomainEvent event);

}
